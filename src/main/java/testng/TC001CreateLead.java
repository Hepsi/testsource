package testng;


import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import wdMethods.ProjectMethods;
import week6.ReadExcel;

public class TC001CreateLead extends ProjectMethods{
	
	@BeforeTest
		public void setData() {
			testCaseName = "TC001_CreateLead";
			testDesc = "Create A new Lead";
			author = "gopi";
			category = "smoke";
		}
	
	
	@Test(dataProvider="qa")
	public void createLead(String companyName, String firstName, String lastName, String email, String ph) {
		
		WebElement cl = locateElement("link", "Create Lead");
		click(cl);
		type(locateElement("createLeadForm_companyName"), companyName);
		type(locateElement("createLeadForm_firstName"), firstName);
		type(locateElement("createLeadForm_lastName"), lastName);
		type(locateElement("createLeadForm_primaryEmail"), email);
		type(locateElement("createLeadForm_lastName"), ph);
		click(locateElement("class", "smallSubmit"));
	}
	@DataProvider(name="qa")
	public Object[][] fetchData() throws IOException {
		Object[][] data = ReadExcel.readExcel();
		return data;
	}
	
}
